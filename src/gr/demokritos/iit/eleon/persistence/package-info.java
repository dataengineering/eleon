/**
 * Provides the classes necessary to persist ELEON-edited data in a variety of backends.
 * 
 */
package gr.demokritos.iit.eleon.persistence;
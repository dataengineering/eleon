/**
 * Provides the classes that implement the facet interfaces for annotated datasets.
 * 
 * @see gr.demokritos.iit.eleon.facets
 */

package gr.demokritos.iit.eleon.facets.dataset;
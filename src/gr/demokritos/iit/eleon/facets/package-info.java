/**
 * Provides the interfaces for implementing facets for presenting data for annotation. 
 * 
 */

package gr.demokritos.iit.eleon.facets;
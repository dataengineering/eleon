package gr.demokritos.iit.indigo.adaptation;

public class UMException extends Exception {
    public UMException() {
        super();
    }
    public UMException(String msg) {
        super(msg);
    }
}